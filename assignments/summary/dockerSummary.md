When Do You Need to Use a Docker?

For replicating the environment on your server, while running your code locally on your laptop
For Docker CI/CD during numerous development phases (dev/test/QA)
For distributing your app’s OS with a team, and as a version control system.


How Do You Setup a Docker Locally

Download a Docker edition and the Docker Toolbox
Make sure your BIOS has Virtualization Technologies, AMD-V, or KVM enabled
Install the Extension Pack in the Oracle VirtualBox.
Run the Setup



How Do You Use a Docker?

The biggest advantage of VMs is that they create snapshots which can be revisited instantly later. Docker containers further enhance the lightweight process virtualization by being OS independent and using the Linux Kernel’s functionality. They are created from Docker images – like snapshots. Docker images are created using a Docker file which can be customized or used as is. The default execution driver for creating a docker container is ‘libcontainer’.  Docker Hub can be used for searching docker images and seeing the way they have been built.

To create a Docker container, download the ‘hello world’ image, by typing the following command in the terminal –
$ docker run hello world

For checking the number of images on your system, use the following command –
$ docker images

For searching an image in the Docker Hub –
$ docker search <image>

Here’s a List of Docker Commands



docker run – Runs a command in a new container.
docker start – Starts one or more stopped containers
docker stop – Stops one or more running containers
docker build – Builds an image form a Docker file
docker pull – Pulls an image or a repository from a registry
docker push – Pushes an image or a repository to a registry
docker export – Exports a container’s filesystem as a tar archive
docker exec – Runs a command in a run-time container
docker search – Searches the Docker Hub for images
docker attach – Attaches to a running container
docker commit – Creates a new image from a container’s changes
Check out the complete list of commands in the Docker documentation

 

Examples of Using a Docker

You can run WordPress locally on your laptop by downloading Docker, without having to install Apache, PHP, MySQL etc. The Docker Toolbox creates a containerized version of Linux to run the Docker in a VM.
Download Docker Tool Box which will install the Oracle VirtualBox.
Install the Extension Pack in the VirtualBox.
Type $ docker run hello-world in the terminal to check if your installation has finished properly.
Search for a WordPress image on the Docker Hub to install WordPress locally.
Similarly, you can install DokuWiki using dockers.
Dockers can be used for testing SDN components